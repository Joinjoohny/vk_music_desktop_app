// @flow
import * as storage from 'redux-storage';
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import auth from './auth';

const rootReducer = storage.reducer(combineReducers({
  auth,
  routing
}));

export default rootReducer;
