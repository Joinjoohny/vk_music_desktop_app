// @flow
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import Login from './components/Login';

const requireAuth = (store) => {
  const isLoggedIn = store.getState().auth.token !== null;
  console.log(store.getState(), 'requireAuth');
  if (!isLoggedIn) {
    console.log('not login');
    return (nextState, replace) => {
      replace('/login/');
    };
  }
};

export default (store: Object) => (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} onEnter={requireAuth(store)} />
    <Route path="/home" component={HomePage} />
    <Route path="/login" component={Login} />
  </Route>
);
