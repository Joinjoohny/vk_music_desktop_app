import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { hashHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import * as storage from 'redux-storage';
import createEngine from 'redux-storage-engine-localstorage';
import filter from 'redux-storage-decorator-filter';
import createLogger from 'redux-logger';
import rootReducer from '../reducers';
import { LOGIN } from '../actions/login';

// import * as counterActions from '../actions/counter';

// const actionCreators = {
//   ...counterActions,
//   push,
// };

const logger = createLogger({
  level: 'info',
  collapsed: true
});

const router = routerMiddleware(hashHistory);

// If Redux DevTools Extension is installed use it, otherwise use Redux compose
/* eslint-disable no-underscore-dangle */
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
//   window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
//     // Options: http://zalmoxisus.github.io/redux-devtools-extension/API/Arguments.html
//     actionCreators,
//   }) :
//   compose;


export default function configureStore(initialState: Object) {
  const engine = filter(
    createEngine('auth_vk'),
    [['auth', 'token']],
  );

  const storageMiddleware = storage.createMiddleware(engine, [], [LOGIN.SUCCESS]);

  const createStoreWithMiddleware = applyMiddleware(
    thunk,
    router,
    logger,
    storageMiddleware
  )(createStore);

  const store = createStoreWithMiddleware(rootReducer, initialState);
  // Load settings from localStorage
  const load = storage.createLoader(engine);

  load(store)
    .then((nextState) => {
      // Check if the user is logged in
      console.log(store.getState(), 'load');
      const isLoggedIn = store.getState().auth.token !== null;
      if (isLoggedIn) {
        return;
      }
    }).catch(err => {
      console.log(err);
    });

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers')) // eslint-disable-line global-require
    );
  }

  return store;
}
