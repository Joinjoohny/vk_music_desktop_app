// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from './Home.css';
import { loginUser } from '../actions/login';

const electron = window.require('electron');
const ipcRenderer = window.require('electron').ipcRenderer;
const remote = electron.remote;
const BrowserWindow = remote.BrowserWindow;

class Login extends Component {

  componentWillReceiveProps(nextProps) {
    const isLoggedIn = nextProps.token !== null;
    if (isLoggedIn) {
      ipcRenderer.send('reopen-window');
      this.context.router.push('/home');
    }
  }

  requestToken(code) {
    this.props.loginUser(code);
  }

  authVk() {
    const self = this;

    // Build the OAuth consent page URL
    const authWindow = new BrowserWindow({
      width: 800,
      height: 600,
      show: true,
      webPreferences: {
        nodeIntegration: false
      }
    });

    // Get code
    const vkUrl = 'https://oauth.vk.com/authorize?';
    const authUrl = `${vkUrl}client_id=5725796&scope=audio&redirect_uri=https://oauth.vk.com/blank.html&display=popup&revoke=1`;
    authWindow.loadURL(authUrl);

    function handleCallback(url) {
      const rawCode = /code=([^&]*)/.exec(url) || null;
      const code = (rawCode && rawCode.length > 1) ? rawCode[1] : null;
      const error = /\?error=(.+)$/.exec(url);

      if (code || error) {
        // Close the browser if code found or error
        authWindow.destroy();
      }

      // If there is a code, proceed to get token from github
      if (code) {
        console.log(code);
        self.requestToken(code);
      } else if (error) {
        alert('Oops! Something went wrong and we couldn\'t ' +
          'log you in using VK. Please try again.');
      }
    }

    // If "Done" button is pressed, hide "Loading"
    authWindow.on('close', () => {
      authWindow.destroy();
    });

    authWindow.webContents.on('will-navigate', (event, url) => {
      handleCallback(url);
    });

    authWindow.webContents.on('did-get-redirect-request', (event, oldUrl, newUrl) => {
      handleCallback(newUrl);
    });
  }

  render() {
    return (
      <div>
        <div className={styles.container}>
          <h2>LOGIN</h2>
          <button onClick={this.authVk.bind(this)}>Login</button>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: React.PropTypes.func.isRequired
};

Login.contextTypes = {
  router: React.PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    token: state.auth.token,
    response: state.auth.response,
    failed: state.auth.failed,
    isFetching: state.auth.isFetching
  };
}

export default connect(mapStateToProps, { loginUser })(Login);
