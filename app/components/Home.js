// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import styles from './Home.css';
import { getAudio } from '../actions/audio';

class Home extends Component {

  getAuduo() {
    this.props.getAudio();
  }

  render() {
    return (
      <div>
        <div className={styles.container}>
          <h2>Home page</h2>
          <Link to="/login">Login</Link>
          <button onClick={this.getAuduo.bind(this)}>Get Audio</button>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  getAudio: React.PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return {
    audio: state.audio
  };
};

export default connect(mapStateToProps, { getAudio })(Home);
