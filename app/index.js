// @flow
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import routes from './routes';
import configureStore from './store/configureStore';
import './app.global.css';

// Store
const store = configureStore();

const history = syncHistoryWithStore(hashHistory, store);

console.log(store.getState(), 'getState!!!');


render(
  <Provider store={store}>
    { /* Tell the Router to use our enhanced history */ }
    <Router history={history}>
      {routes(store)}
    </Router>
  </Provider>,
  document.getElementById('root')
);
