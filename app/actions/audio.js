// @flow
import apiRequest from '../utils/api';
import constants from '../utils/constants';

export function makeAsyncActionSet(actionName: any) {
  return {
    REQUEST: `${actionName}_REQUEST`,
    SUCCESS: `${actionName}_SUCCESS`,
    FAILURE: `${actionName}_FAILURE`
  };
}

// Authentication
export const GET_AUDIO = makeAsyncActionSet('GET_AUDIO');

export function getAudio() {
  return (dispatch: Function, getState: Function) => {
    const data = {
      access_token: getState().auth.token,
      v: constants.VERSION_API
    };
    const url = 'https://api.vk.com/method/audio.get';
    const method = 'POST';

    dispatch({ type: GET_AUDIO.REQUEST });

    return apiRequest(url, method, data)
      .then((response) => {
        dispatch({ type: GET_AUDIO.SUCCESS, payload: response.data });
      })
      .catch((error) => {
        dispatch({ type: GET_AUDIO.FAILURE, payload: error.response.data });
      });
  };
}
