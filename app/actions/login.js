// @flow
import apiRequest from '../utils/api';
import constants from '../utils/constants';

export function makeAsyncActionSet(actionName: any) {
  return {
    REQUEST: `${actionName}_REQUEST`,
    SUCCESS: `${actionName}_SUCCESS`,
    FAILURE: `${actionName}_FAILURE`
  };
}

// Authentication
export const LOGIN = makeAsyncActionSet('LOGIN');

export function loginUser(code: any) {
  return (dispatch: Function) => {
    const data = {
      code,
      client_id: constants.CLIENT_ID,
      client_secret: constants.CLIENT_SECRET,
      redirect_uri: constants.REDIRECT_URI
    };
    const url = constants.OAUTH_ACCESS_TOKEN;
    const method = 'GET';

    dispatch({ type: LOGIN.REQUEST });

    return apiRequest(url, method, data)
      .then((response) => {
        dispatch({ type: LOGIN.SUCCESS, payload: response.data });
      })
      .catch((error) => {
        dispatch({ type: LOGIN.FAILURE, payload: error.response.data });
      });
  };
}
