import axios from 'axios';

const apiRequest = (url, method, params = {}) => {
  axios.defaults.headers.common['Accept'] = 'application/json';
  axios.defaults.headers.common['Content-Type'] = 'application/json';
  axios.defaults.headers.common['Cache-Control'] = 'no-cache';
  return axios({ method, url, params });
};

export default apiRequest;
